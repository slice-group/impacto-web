var ctrl = {};
(function(){	
	var clickAll = false;
	this.selected = [];
	this.pages = {};
	this.page = 1
	this.interval_a = 0;
	this.interval_b = 10;
	/*
		id = id del registro en la base de datos
		selectorIdRow = el selector jQuery con el id de la etiqueta hmtl del registro a eliminar
		selectorIdDelete = el selector jQuery con el id del boton eliminar
		selectorIdCheck = el selector jQuery con el id del checkbox del registro a eliminar
		slected = array que contiene los ids de los registros seleccionados a eliminar
	*/
	this.itemSelected = function(id, $scope){		
		var found = 0;
		for(var i=0;i<this.selected.length;i++){
			if(this.selected[i]==id){				
				this.selected.splice(i,1);	
				found = 1;
				this.pages[$scope.page] = this.selected;
			}
		}
		if(found==0){
			this.selected.push(id);
			this.pages[$scope.page] = this.selected;
		}
		if(this.selected.length>0){
			$scope.btnDelete = true;
		}else{
			$scope.btnDelete = false;
		}
		console.log(this.selected);
	}

	this.allItemsSelected= function($scope, rows){		
			this.selected=[];			
			$scope.btnDelete = true;
			var i = $scope.interval_a
			var length = $scope.interval_b
			if ($scope.page >= rows.data.length/10) {
				length = i+(10-($scope.interval_b-rows.data.length))
			}
			if(this.pages[$scope.page] == undefined){				
				while (i < length) {
				    rows.data[i].checked = true;
				    this.selected.push(rows.data[i].id);
				    i++;
				}
				this.pages[$scope.page] = this.selected;
			}else{
				console.log(this.pages[$scope.page]);
				while (i < length) {
			    	rows.data[i].checked = false;
			    	i++
				}
				$scope.btnDelete = false;
				delete this.pages[$scope.page];
			}
			console.log(this.pages);
			this.selected=[];	
			for (var k in this.pages) {
			    if (this.pages.hasOwnProperty(k)) {
			      this.selected=this.selected.concat(this.pages[k]);
			    }
			}
	    console.log(this.selected);
	    clickAll = true;
	}
	/* Funcion para aparecer y desaparecer el boton eliminar...
		* selectorIdDelete = id del botton eliminar
		* status = 0: disabled y 1: enabled 
	*/

	this.paginateControl = function($scope, length, direction){
		if(direction=="next"){
			if( $scope.interval_b <  length){
				$scope.interval_a = $scope.interval_a+10;
				$scope.interval_b = $scope.interval_b+10;
				$scope.page = $scope.page + 1;
			}
		}else if("last"){
			if( $scope.interval_a > 0 ){
				$scope.interval_a=$scope.interval_a-10;
				$scope.interval_b=$scope.interval_b-10;
				$scope.page = $scope.page - 1;
			}
		}
		ctrl.interval_a = $scope.interval_a;
		ctrl.interval_b = $scope.interval_b;
		ctrl.page = $scope.page;
	}

	this.pageInit = function($scope){
		this.selected = [] /*incializa el array luego de eliminar*/
	 	this.pages = {};	 	
	 	$scope.btnDelete = false;
	 	if($scope.page!=1 && clickAll==true){	
		 	$scope.interval_a=$scope.interval_a-10;
			$scope.interval_b=$scope.interval_b-10;
			$scope.page = $scope.page-1;
			clickAll = false;
		}
	}

	this.removeItemTable = function(rows){
		for(var i=0; i>this.selected.length; i++){
			rows.data[i].splice(i, 1);
		}
	}

}).apply(ctrl);