angular.module('user').factory('users', [
  '$http', function($http) {
    var employees = {};
    var route_index = '/admin/users/angular_index.json'
    var route_destroy = '/admin/users/delete/'


  employees.load = function() {
    Model.get(route_index, $http, function(output){
      employees.data = output;
    });
  };

  employees.destroy = function(ids, $scope) {
    Model.destroy(route_destroy, $http, $scope, ids, function(output){
      Model.get(route_index, $http, function(output){
        employees.data = output;
      });
    });
  };

  return employees;
  }
]);