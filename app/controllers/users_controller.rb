class UsersController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'

  def index
    authorize! :index, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
  end

  def angular_index
    users = User.user_json(current_user)
    respond_to do |format|
      format.html
      format.json { render :json => users  }
    end
  end

  def new
    authorize! :new, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    unless current_user.has_role? :admin
      redirect_to :back, :alert => t("notification.denied")
    end
  end

  def edit
    authorize! :update, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    @user = User.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"

    @user = User.find(params[:id])

    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to users_path, :notice => t("notification.update") }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    authorize! :create, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        @user.add_role Role.find(user_params[:role_ids]).name
        format.html { redirect_to users_path, notice: t("notification.create") }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :destroy, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => t("notification.delete")
    else
      redirect_to users_path, :notice => t("notification.un_delete")
    end
  end
    
  def delete
    authorize! :destroy, @user, :message => "#{t('cancan.messages.access_denied')} #{t('models.user')}"
    User.destroy( redefine_destroy params[:ids].split(",") )
    respond_to do |format|
      format.json { redirect_to users_path, notice: t("notification.delete") }
    end
  end

  private

  #metodo para redefinir el array de los elementos selecionados que se van a eliminar
  def redefine_destroy(params)
    params.sort.each do |id|
      params.delete id unless User.exists? id
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :role_ids, :encrypted_password)
  end

  

end
