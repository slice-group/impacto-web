class VisitorsController < ApplicationController
	def index
		Location.add(request.remote_ip)
		@catalogo = InyxCatalogueRails::Catalogue.where(public: true)
		datos= @catalogo
		seccion = 3
		items = datos.count
		residuo = datos.count.modulo(seccion)
		secciones = residuo > 0 ? (items/seccion)+1 : items/seccion
		@catalogos = {}
		secciones.times do |x|
			@catalogos[x] = datos[0...seccion]
			datos = datos.drop(seccion)
		end
	end
end
