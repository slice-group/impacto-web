class User < ActiveRecord::Base
  rolify
  validates_presence_of :name, :role_ids
  
	#has_many :posts, dependent: :destroy relation posts
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  

  def self.user_json(current_user)
    json = []
    User.where("id != #{current_user.id}").order('created_at DESC').each do |user|
      u = user.as_json 
      u.merge!({rol: user.roles.first.id })
      json.push u
    end
    json.as_json
  end
  
  def self.group_by_roles
    rtrn = {}
    [:admin, :moderator, :redactor].each do |rol|
      rtrn[rol] = User.with_role(rol).count
    end
    rtrn
  end
end
