# Agregar datos de configuración
ContactUs.setup do |config|
	config.mailer_to = "info@fundaimpacto.com'"
	config.mailer_from = "no-reply@inyxtech.com"
	config.name_web = "Fundación Impacto"
	config.route_send = "/"
	config.active_form = false

	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LfYYf8SAAAAADCE8JWF2c4aDRxV4Q837II0Vnvp"
	  config.private_key = "6LfYYf8SAAAAAAyxl7heBR8hzE2eRaRL7uT5wdh9"
	end
end
