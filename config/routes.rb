Rails.application.routes.draw do
  root to: 'visitors#index'

  mount ContactUs::Engine, :at => '', as: 'contact'
  mount InyxCatalogueRails::Engine, :at => '', as: 'catalogues'
  
  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]
  scope :admin do
  	resources :users do
  		collection do
  			get '/angular_index', to: 'users#angular_index'
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
