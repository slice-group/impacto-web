# This migration comes from inyx_employees_rails (originally 20140901143107)
class CreateInyxEmployeesRailsEmployees < ActiveRecord::Migration
  def change
    create_table :inyx_employees_rails_employees do |t|
      t.string :rif
      t.string :names
      t.string :last_names
      t.string :sex
      t.string :email
      t.string :birth
      t.string :phone
      t.string :mobile
      t.text :adress
      t.string :academic_level
      t.string :profession
      t.text :experience
      t.text :summary_aptitudes
      t.text :summary_courses
      t.text :summary_works

      t.timestamps
    end
  end
end
