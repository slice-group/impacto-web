class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :ip
      t.string :country_code
      t.string :country
      t.float :latitude
      t.float :longitude
      
      t.timestamps
    end
  end
end
